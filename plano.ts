import { Component, Input, ViewChild, ElementRef } from '@angular/core';


import { Dron } from './dron';
import { Posicion } from './posicion';

@Component({
    selector: 'plano',
    templateUrl: './templates/plano.html'
})

export class PlanoComponent {
    dron: Dron;


    @ViewChild('canvasElem') canvas: ElementRef;

    constructor() {
        this.dron = new Dron();
        this.dron.limite = 10;
        this.resetDron();
    }


    public resetDron(): void {
        this.dron.posicion = { x: 0, y: 0, direccion: 'N' };
    }

    // Calcula las rutas
    public procesar(rutas: string[]): string[] {
        this.resetDron();

        var posicionesRespuesta: string[] = [];

        console.log('Reinicia Dron');
        console.log('procesar en plano');
        for (let rutaN of rutas) {
            rutaN = rutaN.trim();
            console.log('rutaN', rutaN);
            for (let i = 0; i < rutaN.length; i++) {
                console.log('Accion', rutaN[i]);
                this.dron.mover(rutaN[i]);
            }

            var posicionFinal = this.dron.posicion;
            console.log(posicionFinal);

            let txtDir = this.textoDireccion(posicionFinal.direccion);
            posicionesRespuesta.push('(' + posicionFinal.x + ', ' + posicionFinal.y + ') dirección: ' + txtDir);
        }
        console.log(posicionesRespuesta);
        return posicionesRespuesta;
    }

    // Calcula la palabra completa de la dirección
    private textoDireccion(letraDireccion: string): string {
        let txtDir = 'N/A';
        switch (letraDireccion) {
            case 'N': txtDir = 'Norte';
            case 'N': txtDir = 'Norte';
            case 'N': txtDir = 'Norte';
            case 'N': txtDir = 'Norte';
        }
        return txtDir;
    }
}
