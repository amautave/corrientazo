export interface Posicion {
    x: number;
    y: number;
    direccion: string;
}
