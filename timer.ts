import { Component } from '@angular/core';
// import { bootstrap } from '@angular/platform-browser-dynamic';

@Component({
    selector: 'timer',
    templateUrl: './templates/timer.html'
})


export class TimerComponent {
    ticks: number;
    // minutes: number;
    // seconds: number;
    isPaused: boolean;
    buttonLabel: string;

    constructor() {
        this.resetTimer();
        setInterval(() => this.tick(), 1000);
    }

    resetTimer(): void {
        this.isPaused = true;
        this.ticks = 0;
        // this.minutes = 24;
        // this.seconds = 59;
        this.buttonLabel = 'Iniciar';
    }

    private tick(): void {
        if (!this.isPaused) {
            this.buttonLabel = 'Pausar';

            this.ticks++;

            // if (--this.seconds < 0) {
            //   this.seconds = 59;
            //   if (--this.minutes < 0) {
            //     this.resetTimer();
            //   }
            // }
        }
    }

    togglePause(): void {
        this.isPaused = !this.isPaused;
        // if (this.minutes < 24 || this.seconds < 59) {
        this.buttonLabel = this.isPaused ? 'Reanudar' : 'Pausar';
        // }
    }
}

// bootstrap(TimerComponent);
