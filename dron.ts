import { Posicion } from './posicion';

export class Dron {
    posicion: Posicion;
    limite: number;

    constructor() {
        this.posicion = {
            x: 0,
            y: 0,
            direccion: 'N'
        };
        this.limite = 10;
    }

    public mover(letra: string): void {
        if (letra == 'A') {
            this.avanzar();
        } else if (letra == 'I') {
            this.girarIzq();
        } else if (letra == 'D') {
            this.girarDer();
        } else {
            console.error('Movimiento no existente');
        }
    }

    // Girar en sentido contrario de las manecillas del reloj
    private girarIzq(): void {
        if (this.posicion.direccion == 'N') {
            this.posicion.direccion = 'O';
        } else if (this.posicion.direccion == 'O') {
            this.posicion.direccion = 'S';
        } else if (this.posicion.direccion == 'S') {
            this.posicion.direccion = 'E';
        } else if (this.posicion.direccion == 'E') {
            this.posicion.direccion = 'N';
        }
    }

    // Girar en sentido de las manecillas del reloj
    private girarDer(): void {
        if (this.posicion.direccion == 'N') {
            this.posicion.direccion = 'E';
        } else if (this.posicion.direccion == 'O') {
            this.posicion.direccion = 'N';
        } else if (this.posicion.direccion == 'S') {
            this.posicion.direccion = 'O';
        } else if (this.posicion.direccion == 'E') {
            this.posicion.direccion = 'S';
        }
    }

    // Mover el dron hacia adelante según la dirección
    private avanzar(): void {
        if (this.posicion.direccion == 'N') {
            this.posicion.y += 1;
            if (this.posicion.y > this.limite) {
                console.error('El domicilio no es posible fuera del barrio');
                this.posicion.y -= 1;
            }
        } else if (this.posicion.direccion == 'O') {
            this.posicion.x -= 1;
            if (this.posicion.x < -this.limite) {
                console.error('El domicilio no es posible fuera del barrio');
                this.posicion.x += 1;
            }
        } else if (this.posicion.direccion == 'S') {
            this.posicion.y -= 1;
            if (this.posicion.y < -this.limite) {
                console.error('El domicilio no es posible fuera del barrio');
                this.posicion.y += 1;
            }
        } else if (this.posicion.direccion == 'E') {
            this.posicion.x += 1;
            if (this.posicion.x > this.limite) {
                console.error('El domicilio no es posible fuera del barrio');
                this.posicion.x -= 1;
            }
        }
    }
}
