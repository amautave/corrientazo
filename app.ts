import { Component, ViewChild } from '@angular/core';
import { bootstrap } from '@angular/platform-browser-dynamic';
import { TimerComponent } from './timer';
import { PlanoComponent } from './plano';
import { Posicion } from './posicion';

@Component({
    selector: 'app',
    directives: [TimerComponent, PlanoComponent],
    templateUrl: './templates/app.html'
})
class AppComponent {
    rutas: string[];
    respuestas: string[];
    file: any;
    contenido: string;
    mensaje: string;

    @ViewChild('barrio') plano: PlanoComponent;

    constructor() {
        this.file = null;
        this.contenido = '';
        this.mensaje = '';
    }

    private reset(): void {
        this.contenido = '';
        this.rutas = null;
        this.respuestas = null;
    }

    private leerArchivo(): void {
        this.reset();

        console.log("App.fileChanged");
        this.file = (<HTMLInputElement>document.getElementById("archivo")).files[0];

        var fileReader = new FileReader();
        fileReader.readAsText(this.file);
        fileReader.onload = (e:FileReaderEvent) => {
            console.log("fileReader.onload");
            // console.log(e.target.result);
            // resolve(e.target.result);
            this.contenido = e.target.result;
            let rutasTmp: string[];
            if (this.contenido.length > 0) {
                rutasTmp = this.contenido.split('\n');
                console.log(this.rutas);
            }

            if (rutasTmp != null && rutasTmp.length <= 3) {
                this.mensaje = "";
                this.rutas = rutasTmp;
                this.procesar();

            } else {
                this.mensaje = "No es posible cargar las rutas, el dron no puede cargar mas de 3 pedidos.";
            }

        }
    }

    public procesar(): void {
        let posicionesRespuesta: string[] = this.plano.procesar(this.rutas);
        console.log(posicionesRespuesta);
        this.respuestas = posicionesRespuesta;
    }

    public animar(): void {

    }

    fileChanged($event): void {
        this.leerArchivo();
    }
}

interface FileReaderEventTarget extends EventTarget {
    result:string
}

interface FileReaderEvent extends Event {
    target: FileReaderEventTarget;
    getMessage():string;
}



bootstrap(AppComponent);
